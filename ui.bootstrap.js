function create_user(){
	var token_post = make_token_key();
	var user_nama = $('#user_nama').val();
	var pass = $('#pass').val();
	var nama = $('#nama').val();
	$.ajax({
		type: "POST",
		url: "system/data_user/create.php",
		data: {'token_key':token_post,'user_nama':user_nama,'pass':pass,'nama':nama},
		success: function(msg) {
			var data = JSON.parse(msg);
			if(data.token_get == token_post){
				swal({title: "Success",text: "Create Account Succsess!",type: "success"});
			}else if(data.token_get == 2){
				swal({title: "Create Account Fail!",text: "Username Already Used!",type: "error"});
			}else if(data.token_get == 1){
				swal({title: "Create Account Fail!",text: "Username & Password Minimun 4 Character!",type: "error"});
			}else{
				swal({title: "Create Account Fail!",text: "Make Sure Username & Password Not Empty!",type: "error"});
			}
		}
	}); 
}
