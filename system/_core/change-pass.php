<?php session_start(); error_reporting(0);?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="../../resource/css/sweetalert.css" rel="stylesheet">
	<link href="../../resource/css/theme/twitter.css" rel="stylesheet">
</head>

<body>
   <script src="../../resource/js/main/jquery.min.js"></script>
   <script src="../../resource/js/main/sweetalert.min.js"></script>
</body>
</html>

<?php
require 'db.php';
require 'csrf.php';
require 'strep.php';
CSRF::init();
if(isset($_REQUEST['module'])){
	$module = $_REQUEST['module'];
	if($module == 'data_user'){
		if(isset($_REQUEST['password']) && isset($_REQUEST['konfirm']) && isset($_REQUEST['tabel_id'])){
			if(!CSRF::validatePost()) {
				unset($_SESSION['limit']);
				session_destroy();
				die('<script>
					swal({title: "Warning",text: "Access Denied!",type: "warning"}, 
					function() {window.location = "../";
					});
				</script>');
			}
			$limit = $_SESSION['limit'];
			if (time() < $limit){		
				}else{
				unset($_SESSION['limit']);
				session_destroy();
				die('<script>
					swal({title: "Warning",text: "Please Login Again!",type: "warning"}, 
					function() {window.location = "../";
					});
				</script>');
			}
			$password = $_REQUEST['password'];
			$konfirm = $_REQUEST['konfirm'];
			$password = cek_string($password);
			$konfirm = cek_string($konfirm);
			$tabel_id = $_REQUEST['tabel_id'];
			if(!empty($password) ||!empty($konfirm) || !empty($tabel_id)){	
			}else{
				die('<script>
				swal({title: "Warning",text: "Please Fill All Data!",type: "warning"}, 
				function() {window.location = "../../dashboard/main/";
				});
				</script>');
			}
			if($password == $konfirm){	
			}else{
				die('<script>
				swal({title: "Warning",text: "Password With Confirmation Password Not Same!",type: "warning"}, 
				function() {window.location = "../../dashboard/main/";
				});
				</script>');
			}
			$tabel_id = mysqli_real_escape_string($koneksi, $tabel_id);
			$password = mysqli_real_escape_string($koneksi, $password);
			$password = md5($password);
			$update_data = mysqli_query($koneksi, "UPDATE ".$module." SET pass='".$password."' WHERE tabel_id='".$tabel_id."'");
			if($update_data){
				echo '<script>
						swal({title: "Success",text: "Change Password Success!",type: "success"}, 
						function() {window.location = "../";
						});
					</script>';				
			}else{
				echo '<script>
						swal({title: "Error",text: "Change Password Fail!",type: "error"}, 
						function() {window.location = "../../dashboard/main/";
						});
					</script>';	
			}											
		}else{
			die('<script>
				swal({title: "Warning",text: "Data Not Found!",type: "warning"}, 
				function() {window.location = "../../dashboard/main/";
				});
			/script>');
		}			
	}else{
		die('<script>
			swal({title: "Warning",text: "Module Not Work!",type: "warning"}, 
			function() {window.location = "../../dashboard/main/";
			});
		</script>');	
	}
}else{
	die('<script>
			swal({title: "Warning",text: "Module Not Found!",type: "warning"}, 
			function() {window.location = "../../dashboard/main/";
			});
		</script>');	
}
?>
