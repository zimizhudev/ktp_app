<?php session_start(); error_reporting(0);?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="../../resource/css/sweetalert.css" rel="stylesheet">
	<link href="../../resource/css/theme/twitter.css" rel="stylesheet">
</head>

<body>
   <script src="../../resource/js/main/jquery.min.js"></script>
   <script src="../../resource/js/main/sweetalert.min.js"></script>
</body>
</html>

<?php
require 'db.php';
require 'csrf.php';
CSRF::init();
	if(isset($_REQUEST['delete_id']) && isset($_REQUEST['module'])){
		if(!CSRF::validatePost()) {
			unset($_SESSION['limit']);
			session_destroy();
			die('<script>
				swal({title: "Warning",text: "Access Denied!",type: "warning"}, 
				function() {window.location = "../";
				});
			</script>');
		}
		$limit = $_SESSION['limit'];
		if(time() < $limit){		
		}else{
			unset($_SESSION['limit']);
			session_destroy();
			die('<script>
				swal({title: "Warning",text: "Please Login Again!",type: "warning"}, 
				function() {window.location = "../";
				});
			</script>');
		}
		$id = $_REQUEST['delete_id'];
		$module = $_REQUEST['module'];
		$delete_data = mysqli_query($koneksi, "DELETE FROM s WHERE tabel_id='".$id."'");
		if($delete_data)
		{	
			echo '<script>
				swal({title: "Success",text: "Delete Data Success!",type: "success"}, 
				function() {window.location = "../../dashboard/'.$module.'/";
				});
			</script>';						
		}else{
			echo '<script>
				swal({title: "Error",text: "Delete Data Fail!",type: "error"}, 
				function() {window.location = "../../dashboard/'.$module.'/";
				});
			</script>';	
		}
		
	}else{
		die('<script>
			swal({title: "Warning",text: "Data Not Found!",type: "warning"}, 
			function() {window.location = "../../dashboard/'.$module.'/";
			});
		</script>');
	}
?>