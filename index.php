<?php  session_start();
/* if(isset($_SESSION['demo']))
{
	$demo = $_SESSION['demo'];
	if (time() < $demo){
	}else{
		unset($_SESSION['demo']);
		session_destroy();
		die("<script>window.location = '../../'</script>");
	}
}else{
	die("<script>window.location = '../../'</script>");
} */
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Meta, title, favicons -->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Fhaz Framework</title>
  <script src="resource/js/head-login.js"></script>
    <link href="resource/css/sweetalert.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Modak|Montserrat+Alternates:700" rel="stylesheet">
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name" style="font-family: 'Montserrat Alternates', sans-serif;font-size:60px;letter-spacing: 0px;" align="center">FHAZ</h1>
            </div>
			<p>Login Your Account!</p>
            <form class="m-t" role="form" method="POST" action="system/_core/cek.php">
                <div class="form-group">
                    <input type="text" class="form-control" name="username" placeholder="Username" autofocus required maxlength="16" minlength="4">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password" required maxlength="10" minlength="4">
                </div>
                <!-- <div class="form-group" style="text-align:left;font-size:medium;">
                    <input type="checkbox" name="relogin" value="yes"> Gunakan Relogin!
                </div> -->
				<?php require 'system/_core/csrf.php'; CSRF::init(); print CSRF::tokenInput(); ?>
                <button class="btn btn-primary block full-width m-b"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</button>
            </form>
                <hr>
					<a href="#" class="btn btn-success block full-width m-b" data-toggle="modal" data-target=".tambah" data-backdrop="static" data-keyboard="false"><i class="fa fa-user-plus" aria-hidden="true"></i> Register</a> 
					<p class="m-t"> <small>Fhaz Framework &copy; 2017 -  Build By Fhaz Framework.</small> </p>
        </div>
    </div>
    <?php include "modal.php";?>
    <script src="resource/js/foot-login.js"></script>
    <script src="resource/js/function.js"></script>
    <script src="ui.bootstrap.js" type="text/javascript"></script>
</body>
</html>
